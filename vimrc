
if expand("$TERM") == 'rxvt-unicode-256color'
    let &t_SI = "\033[6 q"
    let &t_EI = "\033[1 q"
    set t_Co=256
    set ttimeout notimeout
    set ttimeoutlen=99
    set timeoutlen=133
    if exists('$VCMPROFILE')
        exe 'call '.expand("$VCMPROFILE").'()'
    endif
endif
