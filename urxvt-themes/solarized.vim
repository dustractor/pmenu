function! SolarizedDark()
    set bg=dark
    let g:solarized_termcolors = 16
    let g:solarized_termtrans = 1
    colorscheme solarized
    let g:airline_theme = 'base16_harmonic16'
    hi Folded term=NONE cterm=NONE ctermbg=NONE ctermfg=16
    hi CursorLine term=NONE cterm=NONE ctermbg=NONE
endfunction
