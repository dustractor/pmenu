function Seoul256Light()
    "25[2-6]
    let g:seoul256_light_background = 233
    colorscheme seoul256-light
    let g:airline_theme = "base16_mocha"
endfunction
