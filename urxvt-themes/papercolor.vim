function! PaperColor()
    set bg=light
    set cursorline
    colorscheme PaperColor
    let g:airline_theme = "PaperColor"
endfunction
