function! Tomorrow()
    set bg=light
    set cursorline
    colorscheme Tomorrow
    hi TabLineFill term=NONE cterm=NONE ctermbg=254 ctermfg=4
    hi TabLine term=NONE cterm=NONE ctermbg=254 ctermfg=4
    hi TabLineSel term=NONE cterm=NONE ctermbg=255 ctermfg=0
endfunction
