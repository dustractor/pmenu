function Seoul256Dark()
    "23[3-9]
    set bg=dark
    let g:seoul256_background = 236
    colorscheme seoul256
    let g:airline_theme = "distinguished"
endfunction
