function Verdelo()
    set cursorline
    colorscheme verdelo
    let g:airline_theme = "verdelo"
endfunction
