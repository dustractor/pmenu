import string
import re

_stpat_re = re.compile(string.Template.pattern)

class Intrinsic:
    env_string = ""
    command = ""
    term = ""
    term_name = ""
    theme = ""
    tname = ""
    tclass = ""
    term_format = "{term_name} {term_args}"
    command_format = "{term_format} {command}"
    shell_format = "sh -c '{env_string} {command_format}'"

intrinsic = Intrinsic.__dict__
