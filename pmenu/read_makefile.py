def rules(fromfile):
    filepath = os.path.expanduser(fromfile)
    if not os.path.isfile(filepath):
        return []
    here = os.path.dirname(os.path.abspath(__file__))
    shellscript = os.path.join(here,"dmkrls.sh")
    proc = subprocess.Popen( [shellscript,filepath],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out,err = proc.communicate()
    out = out.decode().strip()
    return out.splitlines()

