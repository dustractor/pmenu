
import xml.dom.minidom as _dom

def _docattr(d,k):
    try:
        rv = object.__getattribute__(d,k)
    except AttributeError:
        rv = d.createElement(k)
    return rv

_dom.Document.__getattribute__ = _docattr

def _elem_setattrs(e,**a):
    for k,v in a.items():
        e.setAttribute(k,str(v))
    return e

_dom.Element.attrs = _elem_setattrs

_dom.Element.__iadd__ = lambda s,o:[s.appendChild(o),s][-1]

_dom.Element.txt = lambda s,t:[s.appendChild(s.ownerDocument.createTextNode(t)),s][-1]

_dom.Element.__str__ = lambda s:s.toprettyxml().strip()

doc = _dom.Document()

