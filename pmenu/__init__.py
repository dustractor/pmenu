
from .ini import MultiReader

from .args import ns

def main():

    print("__name__:",__name__)

    reader = MultiReader(*ns.config)

    print(reader)

if __name__ == "__main__":
    main()
