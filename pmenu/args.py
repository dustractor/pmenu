import argparse

from . import helpstrings


args = argparse.ArgumentParser()
args.add_argument("config",nargs="*",default=["~/.obmek.ini"],
        help=helpstrings.usage)
ns = args.parse_args()
