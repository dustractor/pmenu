import configparser
import locale
import os
from . import namefrom
from .genxml import doc
from .read_makefile import rules
from .intrinsic_defaults import intrinsic
#keycre = configparser.ExtendedInterpolation._KEYCRE

class Reader(configparser.ConfigParser):
    def __init__(self,path):
        super().__init__(
                allow_no_value=True,
                defaults=intrinsic,
                default_section = "defaults",
                interpolation=configparser.ExtendedInterpolation())
        self.read(path,locale.getpreferredencoding())
        self.optionxform = str

class MultiReader:
    def __init__(self,*paths):
        self.paths = {p:Reader(os.path.expanduser(p)) for p in paths}

