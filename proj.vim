call argloco#TabArgs([
            \	["prj",         "Makefile proj.vim setup.py"],
            \	["pmenu",       "pmenu/__init__.py"],
            \	["ini",         "pmenu/ini.py"],
            \	["genx",        "pmenu/genxml.py"],
            \	["rdmk",        "pmenu/read_makefile.py"],
            \	["intrinsic",   "pmenu/intrinsic_defaults.py"],
            \	["namefrom",    "pmenu/namefrom.py"],
            \	["args",        "pmenu/args.py"],
            \	["helpstrings", "pmenu/helpstrings.py"],
            \	])

BaseHere

nmap <F11> :BasePair<CR>
nmap <F12> :BaseTell<CR>
