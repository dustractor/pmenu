#! /bin/bash
# expects 2 args session-name and window-name

Q="$( tmux has-session -t $1:$2 2>&1 )"
if [[ -z "$Q" ]]; then
    tmux attach -t $1:$2 -d
else
    tmux new -s $1 -n $2 \; splitw -l 1 -d "while true;do clear;date +%X; sleep 1;done"
fi
