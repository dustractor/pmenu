" acquire settings {{{1
if exists('vcm_rulereader_script')
    let s:rulereader = g:vcm_rulereader_script
else
    let s:rulereader_script = '/usr/share/bash-completion/completions/make'
endif

if exists('vcm_rulereader_exec_fmt')
    let s:rulereader_exec_fmt = g:vcm_rulereader_exec_fmt
else
    let s:rulereader_exec_fmt = "bash -c ' source \"%s\" ; sed -nrf <( _make_target_extract_script --) \"%s\" ' "
endif
" }}}

" get list of rules {{{1

function! s:rulesfrom(mkfile)
    let l:rules = []
    if filereadable(a:mkfile)
        echo 'reading rules from: '.a:mkfile
        let l:cmd = printf(s:rulereader_exec_fmt,s:rulereader_script,a:mkfile)
        let l:xec = system(l:cmd)
        let l:rules = split(l:xec)
    endif
    echo l:rules
    return l:rules
endfunction

" }}}

let s:vtm = {
            \ 'based': 0,
            \ 'name': '',
            \ 'path': '',
            \ 'makemaker': '',
            \ 'tellmaker': '',
            \ 'rule': 'test',
            \ 'makerscript': expand('<sfile>:p:h:h') . '/bin/tmux.session.sh'
            \ }

function! s:vtm.Base(path) dict
    let self.name = fnamemodify(a:path,":t")
    let self.path = a:path
    let self.makemaker = printf("sh -c 'urxvt -cd %s -e %s %s %sMaker' &",self.path,self.makerscript,self.name,self.name)
    let self.tellmaker = printf("tmux send-keys -t %s:%sMaker.0 make space %s Enter",self.name,self.name,self.rule)
    let self.based = 1
endfunction

function! s:vtm.Tell() dict
    if self.based == 1
        silent! call system(self.tellmaker)
    endif
endfunction

function! s:vtm.Pair() dict
    if self.based == 1
        silent! call system(self.makemaker)
    endif
endfunction

function! s:vtm.SetRule(rule) dict
    let self.rule = a:rule
    call self.Base(self.path)
endfunction

" exposed actions {{{1

function! vtm#BaseHere()
    call s:vtm.Base(getcwd())
endfunction

function! vtm#MakePair()
    call s:vtm.Pair()
endfunction

function! vtm#ChooseRule()
    let l:choices = s:rulesfrom(getcwd()."/Makefile")
    let l:prmtlst = deepcopy(l:choices)
    call map(l:prmtlst,'v:key . " " . v:val')
    let l:rule = l:choices[inputlist(l:prmtlst)]
    call s:vtm.SetRule(l:rule)
endfunction

function! vtm#TellPair()
    call s:vtm.Tell()
endfunction


" }}}
