
let s:save_cpo = &cpo
set cpo&vim

if exists('did_vtm') || &cp || version < 700
    finish
endif
let did_vtm = 1

com BaseHere call vtm#BaseHere()
com BasePair call vtm#MakePair()
com BaseTell call vtm#TellPair()

let &cpo = s:save_cpo
unlet s:save_cpo

